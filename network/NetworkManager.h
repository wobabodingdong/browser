#ifndef NETWORKMANAGER
#define NETWORKMANAGER

#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string>
#include <vector>
#include <stdexcept>

//Temp includes
#include <iostream>

class NetworkManager
{
    public:
        //Constructor and desctructor
        NetworkManager();
        ~NetworkManager();

        std::string HTTPGetRequest(std::string URL);

    private:
        //Variables
        std::vector<int> sockets;

        //Void functions
        void setupSocket();
        void closeSockets();
};

#endif
