#include "NetworkManager.h"

/*
*******************************************************
Public functions
*******************************************************
*/

NetworkManager::NetworkManager()
{
    
}

NetworkManager::~NetworkManager()
{
    closeSockets();
}

std::string NetworkManager::HTTPGetRequest(std::string URL)
{

    return "";
}

/*
*******************************************************
Private functions
*******************************************************
*/


/*
Takes in arguments for creating a new socket.
Arguments:
*/
void NetworkManager::setupSocket()
{
    constexpr int FAIL_CREATING_SOCKET{1};

    sockets.push_back(socket(AF_INET, SOCK_STREAM, 0));

    if(sockets.back() == FAIL_CREATING_SOCKET)
    {
        throw std::runtime_error("Could not create socket.");
    }
}

void NetworkManager::closeSockets()
{
    for(auto i : sockets)
        close(i);
}