#include "network/NetworkManager.h"
#include <thread>
#include <iostream>
#include <string>

int main()
{
    NetworkManager networkmanager = NetworkManager();    
    
    std::string input;
    while(std::cin >> input)
    {
        networkmanager.HTTPGetRequest(input);
    }

    return 0;
}